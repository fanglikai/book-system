import React from 'react';
import {Route} from 'react-router';
import {AppContainer} from './components/app';
// import {AppContainer} from './components/test/index';

export const routes = <Route path="/" component={AppContainer}>
  <Route path="/borrower" component={AppContainer} />
</Route>
