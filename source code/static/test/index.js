import React from 'react';
import { connect } from 'react-redux';

class BorrowerManagement extends React.Component {
 constructor(props){
   super(props);
   this.state = {name: "",address:"",phone:"",ssn:"",message:"",success:null };
   this.handleNameChange = this.handleNameChange.bind(this);
   this.handleSsnChange = this.handleSsnChange.bind(this);
   this.handleAddressChange = this.handleAddressChange.bind(this);
   this.handlePhoneChange = this.handlePhoneChange.bind(this);
   this.handleSubmit = this.handleSubmit.bind(this);
 }

 componentDidMount(){
 }

 componentDidUpdate(prevProps,prevState){
 }

 handleNameChange(event){
    this.setState({name: event.target.value});
 }

 handleSsnChange(event){
    this.setState({ssn: event.target.value});
 }

 handleAddressChange(event){
    this.setState({address: event.target.value});
 }

 handlePhoneChange(event){
    this.setState({phone: event.target.value});
 }

 handleSubmit(event){
   event.preventDefault()
   let borrowerData = this.state
   this.setState({message:'',success:null})
   this.setState({name: '',address:'',ssn:'',phone:''});
   $.ajax({
            url: 'http://localhost:5000/addBorrower',
            type: 'POST',
            data: JSON.stringify(borrowerData),
            success: function(response) {
                this.setState({message:response.message,success:response.success})
            }.bind(this),
            error: function(error) {
                this.setState({message:response.message,success:response.success})
            }.bind(this)
        });

 }

 render(){
   return <div className='borrowerManagement'>111
   </div>
 }
}

export const BorrowerManagementContainer = BorrowerManagement;
