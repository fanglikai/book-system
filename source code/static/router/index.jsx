import About from '../page/about';
import { AppContainer } from '../components/app';
const router = [
    {
        path: "/about",
        component: About
    },
    {
        path: "/borrower",
        component: AppContainer
    }]
// 路由
export default router
