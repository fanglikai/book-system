import React from 'react';
import ReactDOM from 'react-dom';
import { Router, browserHistory } from 'react-router';
import { createHistory } from 'history'
// import { routes } from './routes';
import { createStore, applyMiddleware } from 'redux';
import { fromJS } from 'immutable';
import { Provider } from 'react-redux';
import reducers from './reducers/index';
import thunkMiddleware from 'redux-thunk';
import { Map } from 'immutable';
import { Route } from 'react-router';
// import About from './page/about';
// import { AppContainer } from './components/app';
import reactRouter from './router/index'
let initialState = Map();

const store = createStore(reducers, initialState, applyMiddleware(
   thunkMiddleware
));
const history = createHistory(browserHistory)
ReactDOM.render(
   <Provider store={store}>
      <Router history={browserHistory}>
         {/* {routes} */}
         {/* <Route path="/about" component={About} />
         <Route path="/borrower" component={AppContainer} /> */}
            {
               reactRouter.map((item, index) => {
                  return <Route key={index} path={item.path} component={item.component}></Route>
               })
            }

      </Router>
   </Provider>,
   document.getElementById('app'));
